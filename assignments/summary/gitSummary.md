# Summary for  Git Basics

## Git

Git is a version controlled software that is used now a days by the software developors to make the collaboration simple and easy.

It keeps the track of every changes made by the team members.

## Important terminologies related to Git

#### Repository:
 Often called a _'repo'_ is equivalent to files and folders in our computers.

#### Gitlab:
Second most popular storage place for git repo.

#### Commit:
Similar to save a file.

#### Push:
syncing all commited files to gitlab.

#### Branch:
If we consider the git repo as a folder, several sub folders inside the main folder (git repo) are treated as branch


#### Merge:
When a branch becomes error free it is merged with the master branch.

#### Clone:
Copying the exact online repository at the local machine.

#### Fork:
Creates duplicate under own name.

[Here is a summary of git workflow](https://towardsdatascience.com/general-git-workflow-94cb22d1f18a)

